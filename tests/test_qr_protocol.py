import tempfile
import hashlib

TEST_FILE_NAME = "tests/files/test.jpg"
TEST_FILE_SHA256 = "9d7b6ecca64c7ff8bcdf0de668093106c64b9137c141c5c78eb8e069b725dd87"
TEST_FILE_GIF_SHA256 = "f86e8456e025e54b12032df7a8fb5f9c65003b538d0b2bc37f8fb707268306b1"


class TestQRProtocol:

    def test_encode(self):
        import qr_protocol
        with tempfile.TemporaryDirectory() as tmp_dir_name:
            output_name = f"{tmp_dir_name}/qr.gif"
            qr_protocol.qr_protocol.encode(TEST_FILE_NAME, output_name, file=True)
            with open(output_name, "rb") as gif:
                gif_sha256 = hashlib.sha256(gif.read()).hexdigest()
                assert gif_sha256 == TEST_FILE_GIF_SHA256

    def test_decode(self):
        import qr_protocol
        with tempfile.TemporaryDirectory() as tmp_dir_name:
            output_name = f"{tmp_dir_name}/qr.gif"
            qr_protocol.qr_protocol.encode(TEST_FILE_NAME, output_name, file=True)
            qr_protocol.qr_protocol.decode(output_name)
            with open("restored.jpg", "rb") as restored:
                restored_sha256 = hashlib.sha256(restored.read()).hexdigest()
                assert restored_sha256 == TEST_FILE_SHA256
