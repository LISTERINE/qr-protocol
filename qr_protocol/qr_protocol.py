from PIL import Image
from pyzbar import pyzbar
from tqdm import tqdm
import qrcode

# from concurrent.futures import ThreadPoolExecutor as Pool
from concurrent.futures import ProcessPoolExecutor as Pool
from concurrent.futures import as_completed
import argparse
import base64
import io
import itertools
import sys
import tempfile


QR_MAX_BIT_SIZE = 7089 // 8


def chunks(file_object, chunk_size=QR_MAX_BIT_SIZE):
    """Lazy function (generator) to read a file piece by piece.

    Args:
        file_object (io.IOBase): File like object to read chunks from.
        chunk_size (int): Size of chunks to read (in bytes).

    Yields:
        str: base64 encoded chunk
    """
    index = 0
    while True:
        data = file_object.read(chunk_size)
        if not data:
            break
        yield base64.b64encode(data), index
        index += 1


def calculate_chunk_count(file_size, chunk_size):
    """Figure out how many chunks are in a number of bytes.

    This is basically a way to avoid importing math. Don't be like me.

    Args:
        file_size (int): Number of bytes to chunk.
        chunk_size (int): Size of a chunk.

    Returns:
        int: how many chunks the given byte count can be broken into.
    """
    return float(file_size + (chunk_size - 1)) // chunk_size


def load(data, file=False):
    """Ensure incoming data is converted into a file-like object.

    Args:
        data (str): Data or file path to load.
        file (str): Is this a filepath?
    Returns:
        io.BytesIO: data in a file-like container.
    """
    if not file:
        try:
            data = data.encode()
        except AttributeError:
            pass
        data = io.BytesIO(data)
    return data


def build_image(data, index, output_dir):
    """Create a qrcode from provided data.

    Args:
        data (str): Data to convert into a qr code.
        index (int): The index of this segment in relation to the entire input.
        output_dir (str): Directory to save individual qr image to.

    Returns:
        str: Path to the created qr image.
        int: The index of this segment in relation to the entire input.
    """
    qr = qrcode.QRCode(error_correction=qrcode.constants.ERROR_CORRECT_L)
    qr.add_data(data)
    qr.make()
    img = qr.make_image()
    file_name = f"{output_dir}/{index}.png"
    img.save(file_name)
    return file_name, index


def encode(data, output="qr.gif", file=False):
    """Encode data into a gif of qr codes.

    Args:
        data (str): Data or file path to load.
        output (str): Name of the output qr code gif.
        file (str): Is this a filepath?
    """
    pool = Pool(max_workers=4)
    futures = {}
    complete = []
    with tempfile.TemporaryDirectory() as tmp_dir_name:
        with open(load(data, file), 'rb') as target_data:
            with Pool(max_workers=4) as pool:
                futures = {pool.submit(build_image, *task, tmp_dir_name): task for task in chunks(target_data)}
        done = []
        for future in as_completed(futures):
            x = future.result()
            done.append(x)

        complete = sorted([fut.result() for fut in as_completed(futures)], key=lambda x: x[1])
        images = [Image.open(image_tuple[0]).convert('P') for image_tuple in complete]
        kwargs = {}
        if images:
            if len(images) > 1:
                kwargs["append_images"] = images[1:]
            images[0].save(output, save_all=True, **kwargs)


def decode(image_file, output="restored.jpg", file=False):
    """Decode a qr code gif back into data.

    Args:
        image_file (str): Path to the gif.
        output (str): output file to restore data to.
        file (str): Is this a filepath?
    """
    if file:
        restored = open(output, 'wb')
    else:
        restored = sys.stdout
    try:
        image = Image.open(image_file)
        for frame_index in range(0, image.n_frames):
            image.seek(frame_index)
            data = pyzbar.decode(image)
            for message in data:
                restored.write(base64.b64decode(message.data))
    except Exception:
        if file:
            restored.close()


def main():
    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers()
    encode_parser = subparsers.add_parser("encode")
    encode_parser.set_defaults(directive="encode")
    decode_parser = subparsers.add_parser("decode")
    decode_parser.set_defaults(directive="decode")

    parser.add_argument("input")
    parser.add_argument("output")

    parser.add_argument("--file", action="store_true")
    args = parser.parse_args()

    if args.directive == "encode":
        encode(args.input, output=args.output, file=args.file)
    if args.directive == "decode":
        decode(args.input, output=args.output, file=args.file)


if __name__ == "__main__":
    main()
