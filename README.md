QR Protocol
------------------

Convert large amounts of data into a gif of QR codes, then extract the data back out again.

# Install:
pip install -r requirements.txt
brew install zbar

# Usage:
### Data to gif:
python qr_protocol.py --encode --file test.jpg qr.gif

or

qrp encode --file test.jpg qr.gif


### Gif to data:
python qr_protocol.py --decode qr.gif restored.jpg

or

qrp decode qr.gif restored.jpg


Input

![Original Image](/test.jpg "Original Image")


Will output the gif:

![Encoded gif](/qr.gif "Encoded gif")

Converting back will output:

![Decoded gif](/restored.jpg "Decoded.jpg")


```
sha256sum test.jpg restored.jpg
9d7b6ecca64c7ff8bcdf0de668093106c64b9137c141c5c78eb8e069b725dd87  test.jpg
9d7b6ecca64c7ff8bcdf0de668093106c64b9137c141c5c78eb8e069b725dd87  restored.jpg
```
