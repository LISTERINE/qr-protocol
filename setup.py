from setuptools import setup, find_packages
from distutils.util import convert_path

install_requires = []
with open("requirements.txt", "r") as req_file:
    requirements = req_file.readlines()
    for line in requirements:
        line = line.split("#")[0].strip()
        if not line or line.startswith("#"):
            continue
        install_requires.append(line)


testing_install_requires = install_requires.copy()
with open("testing_requirements.txt", "r") as req_file:
    requirements = req_file.readlines()
    for line in requirements:
        line = line.split("#")[0].strip()
        if not line or line.startswith("#"):
            continue
        testing_install_requires.append(line)

setup(
    name             = "qr-protocol",
    packages         = find_packages(exclude=["tests"]),
    version          = "1.0.0b1",
    description      = "Convert large amounts of data into a gif of QR codes, then extract the data back out again.",
    author           = "jon Ferretti",
    author_email     = "jon@jonathanferretti.com",
    url              = "https://gitlab.com/LISTERINE/qr-protocol",
    classifiers      = ["Development Status :: 4 - Beta"],
    python_requires  = ">=3.6",
    install_requires = install_requires,
    extras_require   = {
        "testing" : testing_install_requires
    },
    entry_points     = {
        'console_scripts': [
            'qrp = qr_protocol.qr_protocol:main'
        ]
    },
)
